﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CrawlerDemo
{
    class Program
    {
        private static string[] websites = { "https://www.thegioididong.com/dtdd-apple-iphone",
                                             "https://www.dienmayxanh.com/dien-thoai-apple-iphone",
                                             "https://viettelstore.vn/iphone" };
        private static List<CrawlItem> crawledList = new List<CrawlItem>();
        static async Task Main(string[] args)
        {
           await TGDDCrawlerasync();
            //await DMXCralerAsync();
            //await VTSCralerAsync();
            await WSSCralerAsync();
            await FPTSCralerAsync();
            await FSSTDCralerAsync();
            //await TikiCralerAsync();
            await PicoCralerAsync();
            //await NgKCralerAsync();

            Console.WriteLine("-------------------------------------------------------");

            for (int i = 0; i < crawledList.Count; i++)
            {
                string itemString = string.Format("[{0}] {1}\n   - Price: {2}\n   - Link: {3}\n   - Image: {4}", i, crawledList[i].Model, crawledList[i].Price, crawledList[i].Link, crawledList[i].ImageUrl);
                Console.WriteLine(itemString);
            }
            Console.ReadLine();
            
        }

        private static async Task TGDDCrawlerasync()
        {           
            //the url of the page we want to test
            var url = "https://www.thegioididong.com/dtdd-apple-iphone";
            var httpClient = new HttpClient();            
            var htmlDocument = new HtmlDocument();
            var html = await httpClient.GetStringAsync(url);
            htmlDocument.LoadHtml(html);

            // a list to add all the list of cars and the various prices 
            if (crawledList ==null)
            {
                crawledList = new List<CrawlItem>();
            }
            //var cars = new List<Car>();
            //var divs = htmlDocument.DocumentNode.Descendants("ul").FirstOrDefault().Descendants("li").ToList();
            var divs = htmlDocument.DocumentNode.SelectNodes(@"//section[@class='cate cate42 filtered']/ul/li/a");

            foreach (var div in divs)
            {                
                var modelItem = div.Descendants("h3").FirstOrDefault();
                string model = (modelItem == null) ? string.Empty : modelItem.InnerText;
                
                var priceItem = div.Descendants("div").Where(o => o.GetAttributeValue("class", "").Equals("price")).FirstOrDefault();

                string price = (priceItem == null || priceItem.Descendants("strong").FirstOrDefault() == null)
                             ? string.Empty
                             : priceItem.Descendants("strong").FirstOrDefault().InnerText;

                if (string.IsNullOrEmpty(price))
                {
                    price = "Đang cập nhật";
                }

                string link = "https://www.thegioididong.com" + div.GetAttributeValue("href", "");
               
                var imageItem = div.Descendants("img").FirstOrDefault();
                string image = string.Empty;
                if (imageItem != null)
                {
                    string dataorigin = imageItem.GetAttributeValue("data-original", "");
                    image = string.IsNullOrWhiteSpace(dataorigin)
                          ? imageItem.GetAttributeValue("src", "")
                          : dataorigin;
                }

                var car = new CrawlItem
                {    
                    Model = model,
                    Price = price,
                    Link = link,
                    ImageUrl = image
                };

                if (!string.IsNullOrEmpty(car.Model))
                {
                    crawledList.Add(car);
                }
                       
            }
            Console.WriteLine("TGDDCrawlerasync done");
        }

        private static async Task VTSCralerAsync()
        {
            try
            {
                //the url of the page we want to test
                var url = "https://viettelstore.vn/iphone";
                var httpClient = new HttpClient();
                var htmlDocument = new HtmlDocument();
                var html = await httpClient.GetStringAsync(url);
                htmlDocument.LoadHtml(html);

                // a list to add all the list of cars and the various prices 
                if (crawledList == null)
                {
                    crawledList = new List<CrawlItem>();
                }
                var divs = htmlDocument.DocumentNode.SelectNodes(@"//selection[@class='item ProductList3Col_item']");

                foreach (var div in divs)
                {
                    var modelItem = div.Descendants("h2").FirstOrDefault();
                    string model = (modelItem == null) ? string.Empty : modelItem.InnerText;

                    var priceItem = div.Descendants("span").Where(o => o.GetAttributeValue("class", "").Equals("price")).FirstOrDefault();
                    string price = priceItem.InnerText;

                    string link = div.GetAttributeValue("href", "");
                    var imageItem = div.Descendants("img").Where(o => o.GetAttributeValue("id", "").Equals("imgSeo_2")).FirstOrDefault();
                    string image = imageItem.GetAttributeValue("src", "");

                    var car = new CrawlItem
                    {
                        Model = model,
                        Price = price,
                        Link = link,
                        ImageUrl = image
                    };

                    if (!string.IsNullOrEmpty(car.Model))
                    {
                        crawledList.Add(car);
                    }

                }
                Console.WriteLine("VTSCralerAsync done");
            }
            catch (Exception)
            {

            }
        }

        private static async Task DMXCralerAsync()
        {
            //the url of the page we want to test
            var url = "https://www.dienmayxanh.com/dien-thoai-apple-iphone";
            var httpClient = new HttpClient();
            var htmlDocument = new HtmlDocument();
            var html = "";
            try
            {
            html = await httpClient.GetStringAsync(url);
            }
            catch (Exception)
            {
                return;
            }
            htmlDocument.LoadHtml(html);

            // a list to add all the list of cars and the various prices 
            if (crawledList == null)
            {
                crawledList = new List<CrawlItem>();
            }
            //var cars = new List<Car>();
            //var divs = htmlDocument.DocumentNode.Descendants("ul").FirstOrDefault().Descendants("li").ToList();
            var divs = htmlDocument.DocumentNode.SelectNodes(@"//div[@id='product-list']/ul/");

            foreach (var div in divs)
            {



            }
            Console.WriteLine("DMXCralerAsync done");
        }

        private static async Task WSSCralerAsync()
        {
            try
            {
                //the url of the page we want to test
                var url = "https://websosanh.vn/dien-thoai-iphone/cat-89.htm";
                var httpClient = new HttpClient();
                var htmlDocument = new HtmlDocument();
                var html = await httpClient.GetStringAsync(url);
                htmlDocument.LoadHtml(html);

                // a list to add all the list of cars and the various prices 
                if (crawledList == null)
                {
                    crawledList = new List<CrawlItem>();
                }

                // Count total page to navigate
                string lastItem = htmlDocument.DocumentNode.SelectNodes(@"//a[@class='last']").FirstOrDefault().GetAttributeValue("href", "");
                int last = int.Parse(lastItem.Split(new string[2] { "https://websosanh.vn/dien-thoai-iphone/cat-89-p" , ".htm" }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault());
                for (int i = 1; i <= last; i ++)
                {
                    string childUrl = string.Format("https://websosanh.vn/dien-thoai-iphone/cat-89-p{0}.htm", i);
                    html = await httpClient.GetStringAsync(childUrl);
                    htmlDocument.LoadHtml(html);

                    var divs = htmlDocument.DocumentNode.SelectNodes(@"//li[@class='product-item row-col']");

                    foreach (var div in divs)
                    {
                        var modelItem = div.Descendants("h3").FirstOrDefault();
                        string model = (modelItem == null) ? string.Empty : modelItem.InnerText;

                        var priceItem = div.Descendants("span").Where(o => o.GetAttributeValue("class", "").Equals("product-price")).FirstOrDefault();
                        string price = priceItem.InnerText.Split(new string[1] { "Giá từ " }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();

                        string link = div.Descendants("a").FirstOrDefault().GetAttributeValue("href", "");
                        var imageItem = div.Descendants("span").Where(o => o.GetAttributeValue("class", "").Equals("product-img")).FirstOrDefault().Descendants("img").FirstOrDefault();
                        string image = imageItem.GetAttributeValue("src", "");

                        var car = new CrawlItem
                        {
                            Model = model,
                            Price = price,
                            Link = link,
                            ImageUrl = image
                        };

                        if (!string.IsNullOrEmpty(car.Model))
                        {
                            crawledList.Add(car);
                        }

                    }
                }
                Console.WriteLine("WSSCralerAsync done");
            }
            catch (Exception)
            {

            }
        }

        private static async Task FPTSCralerAsync()
        {
            try
            {
                //the url of the page we want to test
                var url = "https://fptshop.com.vn/dien-thoai/apple-iphone";
                var httpClient = new HttpClient();
                var htmlDocument = new HtmlDocument();
                var html = await httpClient.GetStringAsync(url);

                htmlDocument.LoadHtml(html);


                // a list to add all the list of cars and the various prices 
                if (crawledList == null)
                {
                    crawledList = new List<CrawlItem>();
                }
                //var cars = new List<Car>();
                //var divs = htmlDocument.DocumentNode.Descendants("ul").FirstOrDefault().Descendants("li").ToList();
                var divs = htmlDocument.DocumentNode.SelectNodes(@"//div[@class='fs-lpil']");

                foreach (var div in divs)
                {
                    var modelItem = div.Descendants("h3").FirstOrDefault().Descendants("a").FirstOrDefault();
                    string model = (modelItem == null) ? string.Empty : modelItem.InnerText;

                    var priceItem = div.Descendants("div").Where(o => o.GetAttributeValue("class", "").Equals("fs-lpil-price")).FirstOrDefault();
                    string price = priceItem.Descendants("p").FirstOrDefault().InnerText;

                    string link = "https://fptshop.com.vn" + div.Descendants("a").Where(o => o.GetAttributeValue("class", "").Equals("fs-lpil-img")).FirstOrDefault().GetAttributeValue("href", "");
                    var imageItem = div.Descendants("img").Where(o => o.GetAttributeValue("class", "").Equals("lazy")).FirstOrDefault();
                    string image = imageItem.GetAttributeValue("data-original", "");

                    var car = new CrawlItem
                    {
                        Model = model,
                        Price = price,
                        Link = link,
                        ImageUrl = image
                    };

                    if (!string.IsNullOrEmpty(car.Model))
                    {
                        crawledList.Add(car);
                    }

                }
                Console.WriteLine("FPTSCralerAsync done");
            }
            catch (Exception)
            {

            }
        }

        private static async Task FSSTDCralerAsync()
        {
            try
            {
                //the url of the page we want to test
                var url = "https://fstudiobyfpt.com.vn/iphone";
                var httpClient = new HttpClient();
                var htmlDocument = new HtmlDocument();
                var html = await httpClient.GetStringAsync(url);

                htmlDocument.LoadHtml(html);

                // a list to add all the list of cars and the various prices 
                if (crawledList == null)
                {
                    crawledList = new List<CrawlItem>();
                }
                //var cars = new List<Car>();
                //var divs = htmlDocument.DocumentNode.Descendants("ul").FirstOrDefault().Descendants("li").ToList();
                var divs = htmlDocument.DocumentNode.SelectNodes(@"//div[@class='fs-ctitem']");

                foreach (var div in divs)
                {
                    var modelItem = div.Descendants("h3").FirstOrDefault().Descendants("a").FirstOrDefault();
                    string model = (modelItem == null) ? string.Empty : modelItem.InnerText;

                    var priceItem = div.Descendants("p").Where(o => o.GetAttributeValue("class", "").Equals("fs-ctpri")).FirstOrDefault();
                    string price = priceItem.InnerText.Split(new string[1] { "Giá chỉ " }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();

                    string link = "https://fstudiobyfpt.com.vn" + div.Descendants("a").Where(o => o.GetAttributeValue("class", "").Equals("fs-ctimg")).FirstOrDefault().GetAttributeValue("href", "");
                    var imageItem = div.Descendants("img").FirstOrDefault();
                    string image = "https://fstudiobyfpt.com.vn" + imageItem.GetAttributeValue("src", "");

                    var car = new CrawlItem
                    {
                        Model = model,
                        Price = price,
                        Link = link,
                        ImageUrl = image
                    };

                    if (!string.IsNullOrEmpty(car.Model))
                    {
                        crawledList.Add(car);
                    }

                }
                Console.WriteLine("FSSTDCralerAsync done");
            }
            catch (Exception)
            {

            }
        }

        private static async Task TikiCralerAsync()
        {
            try
            {
                //the url of the page we want to test
                var url = "https://tiki.vn/dien-thoai-smartphone/c1795/apple";
                var httpClient = new HttpClient();
                var htmlDocument = new HtmlDocument();
                var html = await httpClient.GetStringAsync(url);

                htmlDocument.LoadHtml(html);

                // a list to add all the list of cars and the various prices 
                if (crawledList == null)
                {
                    crawledList = new List<CrawlItem>();
                }
                //var cars = new List<Car>();
                //var divs = htmlDocument.DocumentNode.Descendants("ul").FirstOrDefault().Descendants("li").ToList();
                var divs = htmlDocument.DocumentNode.SelectNodes(@"//div[@class='product-item       ']");

                foreach (var div in divs)
                {
                    var modelItem = div.Descendants("h3").FirstOrDefault().Descendants("a").FirstOrDefault();
                    string model = (modelItem == null) ? string.Empty : modelItem.InnerText;

                    var priceItem = div.Descendants("p").Where(o => o.GetAttributeValue("class", "").Equals("fs-ctpri")).FirstOrDefault();
                    string price = priceItem.InnerText.Split(new string[1] { "Giá chỉ " }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();

                    string link = div.GetAttributeValue("href", "");
                    var imageItem = div.Descendants("img").FirstOrDefault();
                    string image = "https://fstudiobyfpt.com.vn" + imageItem.GetAttributeValue("src", "");

                    var car = new CrawlItem
                    {
                        Model = model,
                        Price = price,
                        Link = link,
                        ImageUrl = image
                    };

                    if (!string.IsNullOrEmpty(car.Model))
                    {
                        crawledList.Add(car);
                    }

                }
                Console.WriteLine("TikiCralerAsync done");
            }
            catch (Exception)
            {

            }
        }

        private static async Task PicoCralerAsync()
        {
            try
            {
                //the url of the page we want to test
                var url = "https://pico.vn/dien-thoai-di-dong/apple-cid75-ma13";
                var httpClient = new HttpClient();
                var htmlDocument = new HtmlDocument();
                var html = await httpClient.GetStringAsync(url);

                htmlDocument.LoadHtml(html);

                // a list to add all the list of cars and the various prices 
                if (crawledList == null)
                {
                    crawledList = new List<CrawlItem>();
                }
                //var cars = new List<Car>();
                //var divs = htmlDocument.DocumentNode.Descendants("ul").FirstOrDefault().Descendants("li").ToList();
                var divs = htmlDocument.DocumentNode.SelectNodes(@"//div[@class=' product']");

                foreach (var div in divs)
                {
                    var modelItem = div.Descendants("h6").FirstOrDefault().Descendants("a").FirstOrDefault();
                    string model = (modelItem == null) ? string.Empty : modelItem.InnerText;

                    var priceItem = div.Descendants("span").Where(o => o.GetAttributeValue("class", "").Equals("price")).FirstOrDefault();
                    string price = priceItem.InnerText;

                    string link = div.Descendants("a").FirstOrDefault().GetAttributeValue("href", "");
                    var imageItem = div.Descendants("div").Where(o => o.GetAttributeValue("class", "").Equals("product-image")).FirstOrDefault();
                    string image = imageItem.Descendants("img").FirstOrDefault().GetAttributeValue("src", "");
                    if (image == "")
                    {
                        image = imageItem.Descendants("img").FirstOrDefault().GetAttributeValue("data-src", "");
                    }

                    var car = new CrawlItem
                    {
                        Model = model,
                        Price = price,
                        Link = link,
                        ImageUrl = image
                    };

                    if (!string.IsNullOrEmpty(car.Model))
                    {
                        crawledList.Add(car);
                    }

                }
                Console.WriteLine("PicoCralerAsync done");
            }
            catch (Exception)
            {

            }
        }

        private static async Task NgKCralerAsync()
        {
            try
            {
                //the url of the page we want to test
                var url = "https://www.nguyenkim.com/dien-thoai-di-dong-apple-iphone/";
                var httpClient = new HttpClient();
                var htmlDocument = new HtmlDocument();
                var html = await httpClient.GetStringAsync(url);

                htmlDocument.LoadHtml(html);

                // a list to add all the list of cars and the various prices 
                if (crawledList == null)
                {
                    crawledList = new List<CrawlItem>();
                }
                //var cars = new List<Car>();
                //var divs = htmlDocument.DocumentNode.Descendants("ul").FirstOrDefault().Descendants("li").ToList();
                var divs = htmlDocument.DocumentNode.SelectNodes(@"//a[@class='nk-link-product']");

                foreach (var div in divs)
                {
                    var modelItem = div.Descendants("h6").FirstOrDefault().Descendants("a").FirstOrDefault();
                    string model = (modelItem == null) ? string.Empty : modelItem.InnerText;

                    var priceItem = div.Descendants("span").Where(o => o.GetAttributeValue("class", "").Equals("price")).FirstOrDefault();
                    string price = priceItem.InnerText;

                    string link = div.GetAttributeValue("href", "");
                    var imageItem = div.Descendants("div").Where(o => o.GetAttributeValue("class", "").Equals("product-image")).FirstOrDefault();
                    string image = imageItem.Descendants("img").FirstOrDefault().GetAttributeValue("src", "");
                    if (image == "")
                    {
                        image = imageItem.Descendants("img").FirstOrDefault().GetAttributeValue("data-src", "");
                    }

                    var car = new CrawlItem
                    {
                        Model = model,
                        Price = price,
                        Link = link,
                        ImageUrl = image
                    };

                    if (!string.IsNullOrEmpty(car.Model))
                    {
                        crawledList.Add(car);
                    }

                }
                Console.WriteLine("NgKCralerAsync done");
            }
            catch (Exception)
            {

            }
        }

    }
}
